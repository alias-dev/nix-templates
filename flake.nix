{
  description = "Nix flake templates";

  outputs = { self }: {
    templates = {
      rust-cli-tool = {
        path = ./rust-cli-tool;
        description = "Rust CLI tool template using crate2nix";
      };
    };

    defaultTemplate = self.templates.rust-cli-tool;
  };
}
