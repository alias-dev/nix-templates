{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";

    flake-utils.url = "github:numtide/flake-utils";

    rust-overlay.url = "github:oxalica/rust-overlay";
    rust-overlay.inputs.nixpkgs.follows = "nixpkgs";
    rust-overlay.inputs.flake-utils.follows = "flake-utils";

    cargo2nix.url = "github:cargo2nix/cargo2nix";
    cargo2nix.inputs.rust-overlay.follows = "rust-overlay";
    cargo2nix.inputs.nixpkgs.follows = "nixpkgs";
    cargo2nix.inputs.flake-utils.follows = "flake-utils";

    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, flake-utils, rust-overlay, cargo2nix, ... }:
    let
      name = "rust-cli-tool";
      rustChannel = "stable";
    in
    flake-utils.lib.eachDefaultSystem
      (system:
        let

          pkgs = import nixpkgs {
            inherit system;
            overlays = [
              rust-overlay.overlay
              (import "${cargo2nix}/overlay")
            ];
          };

          rustPkgs = pkgs.rustBuilder.makePackageSet' {
            rustChannel = "latest";
            packageFun = import ./Cargo.nix;
          };

          # Dev shell rust packages from the oxalica overlay
          devShellRustPkgs = with pkgs.rust-bin.${rustChannel}.latest; [
            rustc
            cargo
            rls
            rust-analysis
          ];
        in
        rec {
          packages = {
            rust-cli-tool = (rustPkgs.workspace.${name} { }).bin;
          };

          # `nix build`
          defaultPackage = packages.${name};

          # `nix run`
          apps.${name} = flake-utils.lib.mkApp {
            inherit name;
            drv = packages.${name};
          };

          defaultApp = apps.${name};

          # `nix develop`
          devShell = pkgs.mkShell
            {
              inputsFrom = builtins.attrValues self.packages.${system};
              buildInputs = devShellRustPkgs ++ (with pkgs;
                [
                  clang
                  lld
                  nixpkgs-fmt
                  cargo-watch
                  cargo-edit
                ]);
              RUST_SRC_PATH = "${pkgs.rust.packages.${rustChannel}.rustPlatform.rustLibSrc}";
            };
        }
      );
}
