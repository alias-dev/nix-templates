use rust_cli_tool::greeting;

use clap::Parser;
use color_eyre::Result;

#[derive(Debug, Parser)]
struct Args {
    #[clap(short, long, default_value = "World")]
    name: String,
}

fn main() -> Result<()> {
    let args = Args::parse();
    println!("{}", greeting(&args.name));

    Ok(())
}
